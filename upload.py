from ftplib import FTP
import os

def upload(pwd):
    ftp = FTP()
    ftp.set_debuglevel(2)
    ftp.connect('ftp.boucek.cz',21)
    ftp.login('ondra', pwd)
    ftp.cwd("/FTP/school_gpu_upload/")
    f1 = "distances_progress.png"
    f2 = "loss_progress.png"
    fp = open(f1, 'rb')
    ftp.storbinary('STOR %s' % os.path.basename(f1), fp, 1024)
    fp.close()
    fp = open(f2, 'rb')
    ftp.storbinary('STOR %s' % os.path.basename(f2), fp, 1024)
    fp.close()
    ftp.quit()