import torch

class Classifier():
    def __init__(self, data, classes):
        assert len(data) == len(classes), "length of data and labels must match"
        self.data = data
        self.data_size = len(data)
        self.classes = classes

    def classify_knn(self, x, k=5):
        labels = torch.zeros(len(x), dtype=torch.int)
        best = torch.zeros(k, dtype=int)
        distances = torch.ones((2,), dtype=torch.float64)

        for i in range(len(x)):
            distances = distances.new_full((1, k), float("inf")).squeeze()
            for j in range(self.data_size):
                if torch.dist(x[i], self.data[j], 2) < torch.max(distances):
                    best[torch.argmax(distances)] = self.classes[j]
                    distances[torch.argmax(distances)] = torch.dist(x[i], self.data[j], 2)
            labels[i] = torch.mode(best)[0]

        return labels


def create_minibatch(imgs, idxs):
    """
    Create batch of images from given indexes

    param imgs: images, tuple (Tensor(n, 1, 28, 28), label)
    param idxs: indexes of images to exctract, Tensor(size, 3)

    return anchor, positive, negative.. each Tensor(size, 1, 28, 28)
    """
    anchor = torch.ones(len(idxs), 1, 28, 28)
    positive = torch.ones(len(idxs), 1, 28, 28)
    negative = torch.ones(len(idxs), 1, 28, 28)
    for i in range(len(idxs)):
        anchor[i] = imgs[idxs[i][0]][0]
        positive[i] = imgs[idxs[i][1]][0]
        negative[i] = imgs[idxs[i][2]][0]
    return anchor, positive, negative

